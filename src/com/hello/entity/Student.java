package com.hello.entity;

public class Student {
    public Student(int id, String name, String surname) {
		this.id = id;
		this.firstName = name;
		this.lastName = surname;
	}
	private int id;
    private String firstName;
    private String lastName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
     
    
}
